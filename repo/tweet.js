module.exports = {
  create,
  doAddPhoto,
  doLike,
  doUnlike,
  doHashTag,
  doRetweet,
  doReply  
};

async function create(db, tweet) {
  const result = await db.execute(
    `
      insert into tweets (
        user_id, content, type
      ) values (
        ?, ?, ?
      )
    `,
    [tweet.userId, tweet.content, tweet.type]
  );
  return result[0].insertId;
}

async function doAddPhoto(db, addphoto) {
  const result = await db.execute(
    `
      insert into tweet_photos (
        tweet_id, url
      ) values (
        ?, ?
      )
    `,
    [addphoto.tweet_id, addphoto.url]
  );
  return result[0].insertId;
}

async function doLike(db, like) {
  const result = await db.execute(
    `
      insert into tweet_likes (
        user_id, tweet_id
      ) values (
        ?, ?
      )
    `,
    [like.user_id, like.tweet_id]
  );
  return result[0].insertId;
}

async function doUnlike(db, unlike) {
  await db.execute(
    ` delete from tweet_likes
      where user_id = ? and tweet_id = ?
    `,
    [unlike.user_id, unlike.tweet_id]
  );
}

async function doHashTag(db, name) {
  const result = await db.execute(
    `
  insert into hashtags (name) values (?)
`,
    [name]
  );
  return result[0].insertId;
}

async function doRetweet(db, retweet) {
  await db.execute(
  `insert into retweets (
    user_id, tweet_id, content
  ) values (
    ?, ?, ?
  )
`,[retweet.user_id, retweet.tweet_id, retweet.content]
  );
}

async function doReply (db, user){
  const result = await db.execute(`
    insert into tweet_replies (tweet_id, user_id, content) 
    values (?, ?, ?)
  `, [
    user.tweet_id, user.user_id, user.content
  ])
  return result[0].insertId
}
