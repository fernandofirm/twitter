
module.exports = function(pool, tweetRepo) {
  return {
    async create(ctx) {
      const createTweet = ctx.request.body;
      // TODO: validate createTweet
      const getTweet = await tweetRepo.create(pool, createTweet);
      ctx.body = {
        createTweet
      };
    },

    async addPhoto(ctx) {
      const createPhoto = ctx.request.body
      const getPhoto = await tweetRepo.doAddPhoto(pool, createPhoto)
      ctx.body = createPhoto
      
    },
    
    async like(ctx) {
      const createLike = ctx.request.body
      const getLike = await tweetRepo.doLike(pool, createLike)
      ctx.body = createLike
    },
    async unlike(ctx) {
      const id = ctx.params.id;
      const createUnlike = ctx.request.body
      let getUnlike = await tweetRepo.doUnlike(pool, createUnlike);
      ctx.body = createUnlike
    },
    async hashTag(ctx) {
      const createTag = ctx.request.body
      console.log(createTag)
      const getTag = await tweetRepo.doHashTag(pool, createTag['name'])
      ctx.body = createTag
    },
    async retweet(ctx) {
      const createRetweet = ctx.request.body
      console.log(createRetweet)
      const getRetweet = await tweetRepo.doRetweet(pool, createRetweet)
      ctx.body = createRetweet
    },
    async reply(ctx) {
      const createReply = ctx.request.body
      const getReply = await tweetRepo.doReply(pool, createReply)
      ctx.body = createReply
    }

  };
};
